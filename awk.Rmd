# AWK

*awk* is a line-oriented language for text processing such as data extraction and reporting. It was written by Aho, Weinberger and Kernighan in 1977, but much improved since. Its syntax is quite different from the standard bash syntax, but once you get a hang on it you will see how powerful it can be for data manipulation.

**Note:** In this section, we will use the Banthracis proteome `BanthracisProteome.txt` as an example file. You may get the file as follows:

```{bash, eval = FALSE}
# download the zipped file using wget
wget --compression=auto https://bitbucket.org/wegmannlabteaching/bash_lecture/raw/master/Files/BanthracisProteome.txt.gz
# unzip the file
gunzip BanthracisProteome.txt.gz
```


## Basic structure

*awk* commands are embedded in single quotes. They always consist of a *pattern* followed by an {action}, where the pattern specifies when the action should be performed. 
```{bash, eval=TRUE, include = FALSE, error = TRUE}
gunzip -c BanthracisProteome.txt.gz > BanthracisProteome.txt
```


In the following example, we are looking for the pattern "Dan" in the file BanthracisProteome.txt. By default each line containing the pattern is printed. Similar to the *grep* command in *bash*, the search is case sensitive (a search for "DAN" and "Dan" will give different results):
```{bash}
awk '/Dan/'  BanthracisProteome.txt
```

Columns are addressed with the `$` sign. Where `$1`, `$2` and `$3` refer to the first, second and third column, respectively. Every string separated by any whitespace will be considered as a column. This has the advantage that you don't need to specify whether your field delimiter consists of a tab or multiple spaces. `$0` stands for the whole line. Hence, in the example below `{print $0}` is equivalent to the default, where no action is given:
```{bash}
awk '/Dan/ {print $0}' BanthracisProteome.txt
```

```{bash}
awk '/Dan/ {print $1,$3}' BanthracisProteome.txt
```


*awk* understands the basic arithmetic operations: `+`,`-`, `*` and `/` (for addition, subtraction, multiplication and division) and `%` for modulo (also known as Euclidean division or division with remainder).
To concatenate columns, you can simply add a space instead of a comma:
```{bash}
echo "1 2 3 4 5" | awk '{print $1, $2*$3, $3-$4, $5%$2, $1 $3}'
```

As you can see, awk can not only read files, but you can also pipe the standard-output of your command-line to awk and you can pipe *awk*'s output to other bash commands:
```{bash}
echo "1 2 3 4 5" | awk '{print $1, $2*$3, $3-$4, $5%$2, $1 $3}' | sed 's/ / and /g'
```


*awk* allows to execute actions at the beginning and very end of a file / stream with the BEGIN and END blocks. As they are only executed once, they are good places to define variables (BEGIN) or to print a final calculation (END). Note that each print statement prints its own line, so for readability in this example we translate newlines to spaces in the end:
```{bash}
echo "sed awk" | awk 'BEGIN {print "The parrot"} {print} END {print "!"}' | tr '\n' ' '
```
**Note**: `{print}` without specification prints the default, which is `$0` (the whole line).


You can also separate columns by other delimiters with the -F statement:
```{bash}
echo "I-don't-like-awk-exercises-even-though-they're-helpful." | awk -F '-' 'BEGIN {print "But"} {print $1, $3, $4",", $7"."}'  | tr '\n' ' '
```

Or even multiple delimiters:
```{bash}
echo "Why-does_this-sentense-look_so_funny-?" | awk -F '[-_]' '{print $1, $3, $4, $7, $8}'
```

Also, you can sum over columns with the `+=` operator. In this example, we store the sum of all values in column 1 in the variable `sum` and print it in the end:
```{bash, echo -2}
echo -e "1 2\n2 3\n5 6\n10 11"
echo " "
echo -e "1 2\n2 3\n5 6\n10 11" | awk '{sum+=$1}; END {print "sum of first column is " sum}'
```
## Variables in awk

You can define variables within the *awk* command. They can be dynamically defined and contain both numbers and strings. Note that the defined variable is only valid within that specific awk command and will be forgotten after the last single quote.
Multiple actions can be performed in one command and are separated by a semicolon (;):
```{bash}
echo "5 7" | awk '{x=10; print $1*x; x=x+$2; print x}'
```

Variables can be increased by 1 for each entry via the `++` operator. This can easily be used to count lines by starting with `x=0` before the first line (using BEGIN), and increasing `x` with every line. In the following example, we count the lines that match the pattern "ID":
```{bash}
awk 'BEGIN {x=0} /ID/ {++x} END {print x, "Lines"}' BanthracisProteome.txt
```

As mentioned above, the variables within *awk* are only valid within one command. Likewise, bash variables from outside of awk must first be introduced. 

_Attention_: Using a previously undefined variable will not necessarily throw an error, but it will be simply have a zero (numeric) or empty (string) value:
```{bash, error=TRUE}
#trying to access an awk variable outside of awk
echo "5 7" | awk '{x=10; print $1*x; x=x+$2; print x}' 
echo "this variable does not exist: $x"
```

```{bash, error=TRUE}
#trying to access a bash variable inside of awk
a=5
echo "5 7" | awk '{print $1+a, 10-a ; print a}'
```

Instead, bash variables should be passed to awk with the `-v` command before the first single quote:
```{bash}
a="awk"

echo "I like" | awk -v what="$a" '{print $0, what}'
```

## Functions

*awk* comes with a large array of built-in numeric functions, including `sqrt(x)` (square root of x), `log(x)`, `exp(x)` (exponential of x), `cos(x)`, `sin(x)` and `tan(x)`.
```{bash}
echo "10 100" | awk '{print log($1), sqrt($2)}'
```

It also offers built-in functions for string manipulations, including `length()`, `substr()`, `sub()`, `tolower()` and `toupper()`:


**length():** Printing the length of the line, including spaces. 
```{bash}
echo "How long am I?" | awk '{print length($0)}'
```

**substr():** the function substr(a,b,c) takes a string a, starts at position b inside that string, and returns everything inside c characters from there. If c is not given, the whole string is printed:
```{bash}
echo "first second third" | awk '{print substr($1,1,2)}'
echo "first second third" | awk '{print substr($1,1,3)}'
echo "first second third" | awk '{print substr($1,1,4)}'
echo "first second third" | awk '{print substr($1,1)}'
echo "first second third" | awk '{print substr($2,1)}'
echo "first second third" | awk '{print substr($2,3)}'
```


**sub():** replace one string by another:
```{bash}
echo "This is awkward!" | awk '{sub("ward", "", $0); print "No,", $0}'
```


**tolower()** / **toupper():** convert into lower or upper cases:
```{bash}
echo "This is awkward!" | awk '{sub("ward", "", $0); print "No,", tolower($1), $2, toupper($3)}'
```

Of course there are many more functions within *awk*. All available functions are explained in the (very long) man pages!


### Random numbers with awk
Random number generators calculate new random numbers based on the current one deterministically.
To get different output, they need a different starting point, known as seed.
By default, the `rand()` function will always start from the same seed within each started command - so if you repeat the following line multiple times, you will always start from the same point:

```{bash, echo = -2}
echo -e "1" | awk '{print rand()}'
sleep 2
echo -e "1" | awk '{print rand()}'
```

Note: There are different types of *awk*, namely `mawk`, `gawk` and base *awk*. `mawk` will always use a different seed, while `gwak` will always use the same seed. For *awk*, it depends on the specific version that you are using, try out the different commands in your command line and see how they behave.

```{bash, echo = -2}
echo -e "1" | gawk '{print rand()}'
sleep 2
echo -e "1" | gawk '{print rand()}'
```

If you want to be sure to use a different seed every time, you can simply use `srand()`, it will use time as a seed: `srand()` function. :
```{bash, echo = -2}
echo -e "1" | awk 'BEGIN{srand()}{print rand()}'
sleep 2
echo -e "1" | awk 'BEGIN{srand()}{print rand()}'
```

## Extended matching
There are multiple ways of finding a pattern. Whether you want to extract lines where a substring matches anywhere in the line, or if you are looking for an exact match, these are the main basic options:


Return all lines that match the substring anywhere in the line:
```{bash}
awk '/ID/' BanthracisProteome.txt | wc -l
```

Return the lines where the first column (or word) exactly matches the complete search pattern:
```{bash}
awk '$1=="ID"' BanthracisProteome.txt | wc -l
```


Return the lines where the second column exactly matches the complete search pattern:
```{bash}
awk '$2=="ID"' BanthracisProteome.txt | wc -l
```

If you are looking for a pattern anywhere within a specific column, you can use the `~` sign. Be aware of these partial matches, as the search pattern "ID" can also be found within the string "NUCLEOT**ID**E", and the pattern "sample1" also matches the strings "**sample1**1", "**sample1**2", "**sample1**3", ...:
```{bash}
awk '$2 ~ /ID/' BanthracisProteome.txt | wc -l
```

To extract lines based on multiple search patterns at once, you can use `&&` (and) and `||` (or). 

This means, the following command returns all lines that contain either the exact pattern "ID" in the first column, **or** the string "ID" anywhere in the second column:
```{bash}
awk '$1 == "ID" || $2 ~ /ID/' BanthracisProteome.txt | wc -l
```

While the next code returns all lines that contain the exact pattern "ID" in the first column, **and** contain the string "ID" anywhere in the second column:
```{bash}
awk '$1 == "ID" && $2 ~ /ID/' BanthracisProteome.txt | wc -l
```

You can also use regular expressions in your search patterns. Remember the tutorial about regular expressions (chapter 9.4) - the same results can be accomplished by matching for the regex pattern in awk:
```{bash}
echo -e 'TACACACTTTAGAGTTTACAGACTTT' | awk '$1 ~ /(A[CG])+/'
```

## If-Else 
To perform specific actions based on conditions, you can use if-else statements in the form of `if(<condition>){action}`, `if(<condition>){action} else {action}`, or if you want to add multiple else conditions you can extend the command to `if(<condition1>){action1} else if(<condition2>) {action2}`:
```{bash}
awk '{if ($1 == "ID") {++x} else {++y}} END {print x, "lines matched and", y, "lines did not match out of", x+y, "lines"}' BanthracisProteome.txt
```
**Note:** as we learned, undefined variables in *awk* are set to zero or empty at first use. Therefore we did not need to specify x and y in the beginning.

The if-command also makes it possible to store a variable until a condition is met.
As en example imagine a file "zoo_inventory.txt" like that:
```{bash,include = FALSE, error = TRUE}
echo -e "Species: Human\nDaniel\nLiam\nXenia\nSpecies: Chimp\nBetty\nTom\nSpecies: Cat\nNoobie\nNelson" > zoo_inventory.txt
```

```
Species: Human
Daniel
Liam
Xenia
Species: Chimp
Betty
Tom
Species: Cat
Noobie
Nelson
```

So to print each individual with its corresponding species name, we can simply do:
```{bash}
awk '{if($1 ~ /Species/){sp=$2} else {print $1, "("sp")"}}' zoo_inventory.txt
```

## Exercises

```{exercises}
At the beginning it feels awk-ward
```

1) Use `seq` and `tr` to write all numbers from 1 to 1000 on individual lines into a file named `numbers.txt`.

```{solutionBlock}
seq 1 100 | tr ' ' '\n' > numbers.txt
```

2) Use `awk` to extract all lines where the first column is between 107 and 121.

```{solutionBlock}
awk '$1 >= 107 && $1 <= 121' numbers.txt
```

3) Write an `awk` command that replicates `grep "100" numbers.txt`.

```{solutionBlock}
awk '$1 ~ /100/' numbers.txt 
```

4) Use `awk` on `numbers.txt` to create a new file `moreNumbers.txt` that contains the original number in the first column and the log of that number in the second column.

```{solutionBlock}
awk '{print $1, log($1)}' numbers.txt > moreNumbers.txt
```

5) Extend your command from above to write the new file `moreNumbers.txt` with three columns: 1) the original number, 2) the log of that number and 3) either "even" or "odd" depending on whether the original number was even or odd.

```{solutionBlock}
awk '{if($1 % 2){evenOdd = "odd"} else {evenOdd = "even"} print $1, log($1), evenOdd}' numbers.txt > moreNumbers.txt
```

6) Write an `awk` command that counts the number of lines of a file. Test it on `moreNumbers.txt` and compare to `wc -l`.

```{solutionBlock}
wc -l moreNumbers.txt
awk '{++n}END{print n}' moreNumbers.txt
```

7) Use `awk` to calculate the mean of each of the first and second column of `moreNumbers.txt`. Both means should be printed at the end.

```{solutionBlock}
awk '{++n; first += $1; second += $2}END{print first/n, second/n}' moreNumbers.txt
```

8) Restrict the calculation of the mean to rows for which the third column reads "odd" only.

```{solutionBlock}
awk '$3=="odd" {++n; first += $1; second += $2}END{print first/n, second/n}' moreNumbers.txt
```

9) Write an `awk` command that always prints the difference between the values in the second column of two consecutive lines of `moreNumbers.txt`. Omit printing anything for the first line. Your first line should thus be the difference between the second column of the second and first line.

```{solutionBlock}
 awk '$1>1 {print $2-prev} {prev=$2}' moreNumbers.txt
```

10) Write an `awk` command that always prints ten values of the second column of `moreNumbers.txt` onto one line. The first line should thus consist of the values of the first ten lines, the second line those of lines 11 through 20 and so forth.

```{solutionBlock}
awk '{if(n==9){print out $2; out = ""; n=0}else{out = out $2 "\t"; ++n}}' moreNumbers.txt 
```


```{exercises}
awk on the Banthracis proteome
```

**Note:** The following exercises require the Banthracis proteome `BanthracisProteome.txt`. You may get the file as follows:

```{bash, eval = FALSE}
# download the zipped file using wget
wget https://bitbucket.org/wegmannlab/bash_lecture/raw/master/Files/BanthracisProteome.txt.gz
# unzip the file
gunzip BanthracisProteome.txt.gz
```

1) Extract all lines from BanthracisProteome.txt that contain ”ID” in the first column and save them in a new
file ”prots.txt”

```{solutionBlock}
awk '$1 =="ID"' BanthracisProteome.txt > prots.txt
```

2) Use awk to calculate the percentage of them that are ”Reviewed”.

```{solutionBlock}
awk '{++tot; if($3=="Reviewed;") {++x}} END {print 100*(x/tot), "%"}' prots.txt
```

3) Write a BASH script that does the same thing using grep, wc and bc.

```{solutionBlock}
#!/bin/bash
x=$(grep -c "Reviewed" prots.txt)
tot=$(wc -l prots.txt | cut -f1 -d' ')
percent=$(echo "scale=5; 100 * $x / $tot" | bc)
echo "${percent}%"

# solution: 13.36246%
```

4) Use awk to get the total length of all proteins together in amino acids.

```{solutionBlock}
awk '$1=="ID" {tot = tot+$4} END {print tot}' BanthracisProteome.txt

# solution: 1439306
```

5) Use awk to print a file ”len.txt” containing only the name of the protein and its length in amino acids (without
the AA). Then, use awk to print a file ”status.txt” containing only the name of the protein and its status
(e.g. ”Reviewed”) for all proteins that contain only letters (no numbers) in their name. Finally, use join to
create a file called len_status.txt, containing three columns: the protein name, its length and its status.

```{solutionBlock}
awk '$1=="ID" {print $2, $4}' BanthracisProteome.txt | sort > len.txt
awk '$1=="ID" && $2!~/[0-9]/ {print $2, $3}' BanthracisProteome.txt | sort > status.txt
join len.txt status.txt > len_status.txt
```

6) Use awk to create a new file ”seq.txt” that contains one line per protein with three columns:  1) the name, 2) the length, 3) the amino acid sequence (as a single column without spaces). Hint: Remember, that awk reads your file line by line and use the opportunity to store values in a variable until a certain condition is met.

```{solutionBlock}
# if 1st column is ID, store name \& len and empty the seq variable. If 1st column is SQ, set addseq=1. If addseq=1, add sequence to seq. If 1st column is //, set addseq=0 (stop adding sequences) and print.
# The order of these commands is crucial. e.g. if the last two conditions are switched, you would add "//" to the seq.
awk 'BEGIN {addseq=0}; {if ($1 == "ID") {name= $2; len=$4; seq="";} else {if ($1 == "SQ") {addseq=1;} else {if ($1 == "//") {addseq=0; print name, len, seq;} else {if (addseq == 1) {seq = seq $1 $2 $3 $4 $5 $6}}}}}' BanthracisProteome.txt> seq.txt
```

7) Make sure that the output of exercise 6 (seq.txt) is correct. To do so, test if the length of the protein sequence equals the number in column 2. If a line fails this sanity-check, print "ERR: lengths differ for \<the failed line\>". Use the END command to add a last line, either stating "file checked", or "ERR: something went wrong". (You can correct the length of failed proteins and repeat to see if your sanity-check works both-ways).

```{solutionBlock}
awk 'BEGIN {test=0}; {if (length($3) != $2) {print "ERR: lengths differ for " $0; test=1;}}; END {if (test == 1) {print "ERR: something went wrong";} else {print "file checked"}}' seq.txt
```

