# BASH Scripts

```{bash, include = FALSE, error = TRUE}
# remove test.sh, to re-set all file permissions
rm test.sh
```

Until now, we have been executing all BASH commands on the command line. This is perfectly fine for small commands, however, for long and complicated BASH pipelines, we might want to save our work as a BASH script. A BASH script is a plain text file which contains a series of commands. Anything we can run on the command line can be put in a script, and the other way around. 

Saving what we do as scripts allows us to reuse our work on new data sets, to share analysis pipelines with others and - most importantly - to remember later what we have actually done!

Writing scripts in very easy. Let's have a look at a simple example, a script that prints "Hello world!":

```{bash, prompt=FALSE, eval=FALSE}
#!/bin/bash
echo 'Hello world!'
```

A BASH script always starts with the line `#!/bin/bash`. The `#!` in here is called "shebang". It marks the beginning of a script. Right after the shebang is the path to the program (interpreter) that should be used to run the script. In our case, shebang tells the computer to use `/bin/bash` to run the script, which is simply the BASH shell. If our script contained python code instead, this line would need to be modified, such that the computer would interpret the code as python.

To wrap up: each BASH script starts with `#!/bin/bash` to tell the computer that the following code must be interpreted as BASH code.

The rest of the file contains the commands that BASH should execute, in our example printing "Hello world!". Nothing changes compared to the normal command line. If needed, comments can be added to the script. Everything after a `#` will be ignored by BASH (except for the shebang, of course). We can thus modify our script and add a comment:

```{bash, prompt=FALSE, eval=FALSE}
#!/bin/bash

# This is a boring script that prints Hello World
echo 'Hello world!'
```

Let's now discuss how we can write such a BASH script. For this, we will review three common approaches.

## Using `echo`

While tedious, we can write a BASH script line by line using `echo`. We simply use the operators `>` and `>>` to write and append to a file, just like before:

```{bash}
echo '#!/bin/bash' > test.sh
echo "echo 'Hello world'" >> test.sh
```

If you decide to write a BASH script in this way, you will have to comment out all the special characters, as otherwise BASH will interpret them already while writing and not only when executing the file.

Note that it is good practice to use the extension ".sh" for files containing BASH scripts. However, Linux doesn't care about extensions and will also accept a file as a BASH script even if we call it "test.myFantasticFancyFileFormat".

## Using command line text editors

There exist several command line text editors that allow you to modify text files directly from the command line even remotely. Here we will introduce two that are commonly installed on Unix-like systems: `nanp` and `vim`.

### Using `nano`

One of the simplest command-line editors widely available is `nano`. If it is installed on your system, you can start editing a text file "test.sh" with the command

```{bash, eval=FALSE}
nano test.sh
```

Once `nano` is started, you will be able to modify a text file by simply typing and you can even use your arrow keys to move around the file. However, there is no support for your mouse.

To then save your changes, quit nano or many other functionalists, you will need to use key-combinations that involve your `ctrl` key. To write-out (i.e. save) your file, hit `ctrl`+`o`, which will ask you to confirm the file name. To quit, hit `ctrl`+`x`. You can see a list of options on the bottom of the screen. 

### Using `vim`

In 1976, Bill Joy implemented a command-line text editor he called `vi`, which stands for "visual". While powerful, most systems these days ship with `vim`, which is an extension of `vi` and derives its name from "Vi IMproved". 

To start editing a file "test.sh" with `vim`, open a console and type
```{bash, eval=FALSE}
vim test.sh
```

Note that confusingly `vim` is actually started with the command `vi` on some systems, while others only have `vi` - so check on your system which command (`vim`or `vi`) is appropriate.

`vim` offers multiple modes. By default, we enter the *command mode*. We now need to tell `vim` what we want to do:

* Go to *insert mode*: type `i`. We can now enter text. For example, write the two lines of code from the example above. 
* To exit back to *command mode*, press the `Esc` button on the keyboard. 
* When in *command mode*, we can give commands starting with a colon `:`, followed by the command letter, see below for examples. To confirm, press return (the `Enter` button on the keyboard).
  + `:w` saves the file
  + `:q` quits `vim`
  + `:wq` saves the file and quits `vim`
  + `:q!` quits `vim` without saving the file
  + `:s/x/y/g` replaces all occurrences of x with y

Quitting `vim` can be very confusing at first - in fact, the [stackoverflow entry](https://stackoverflow.com/questions/11828270/how-do-i-exit-the-vim-editor) about this question has been visited more than 2.4 million times, which adds up to about [80 people an hour on workdays](https://stackoverflow.blog/2017/05/23/stack-overflow-helping-one-million-developers-exit-vim/) struggling to quit `vim`.

But because of its widespread availability on most system and its rich feature set, `vim` is still one of the most popular text editors today and many codes exclusively use `vim` for all their coding. So it's definitively worth having a look at `vim`, even if it has a somewhat steep learning curve.

## Using a text editor with a GUI

The most convenient option is to use a text editor with a graphical user interface (GUI). The standard editor on Gnome systems in `gedit`:
```{bash, eval=FALSE}
gedit test.sh
```

However, this only works when the connection allows X11 forwarding. X11 forwarding is referring to a mechanism that enables you to run a remote application (such as `gedit`), but forward the output display to your local computer. This is automatically the case when using the terminal on a Linux machine. However, when connecting to a remote computer (e.g. a cluster) with ssh, add the argument `ssh -X` to enable `gedit`. In some cases, this is not possible, so knowing how to use `vim` is still very handy.

### Running a process in the background

By default, any program launched on the command line will become the main process - our command line is "blocked" and does not accept any further commands. Hence we can continue to use the command line only after we closed the program. To launch a program in the background (as a new process), put a `&` after the command:

```{bash, eval=FALSE}
gedit test.sh &
```

Try it out - we can now enter commands on the command line while having the text editor open in the background.

## Execute a BASH script

We have discussed three approaches on how to write BASH code to a file. Let's now see on how to launch the BASH script!

A script can be executed by calling its name

```{bash, error=TRUE}
./test.sh
```

We get a "permission denied" error, since for security reasons, no one has the right to execute a file on Unix by default. We can see the permissions for a files (and directories) by typing

```{bash}
ls -l test.sh
```

Nine columns appear. Those represent the following:

1. column: the file type and the file permissions.
2. column: the number of memory blocks.
3. column: the user (the one who has administrating power).
4. column: the group of the user.
5. column: the file size.
6. column: the month the file/directory was last modified.
7. column: the day the file/directory was last modified.
8. column: the time the file/directory was last modified.
9. column: the name of the file or the directory.

## The Unix permission system

Let us have a closer look at the file permissions. 

Linux allows us to do pretty anything we want. This of course comes along with a lot of dangers - we might delete directories we didn't intend to or uninstall essential programs that are needed for Linux to run. In addition, also malign user inputs (like viruses) can corrupt, change or remove crucial data. To prevent this, there is the Unix permission system that secures the filesystem. It divides authorization into two levels:

1. Ownership
2. Permission

### Ownership

Every file and directory is assigned to three types of owners:

1. *User*: The user is the owner of the file. By default, this is the person who created the file.

2. *Group*: A group can contain multiple users. By default, the group has simply the same name as the user. However, imagine a project where a bunch of people need to access a file. Instead of manually assigning permissions to each user, we could add all users to a group, and then give group permissions to the file, such that all users of this group have the same permissions.

3. *Other*: Any other user who has access to the file. This person is neither user nor part of the group. Essentially, this boils down to "everybody else".

### Permissions

Linux defines permissions for each of the three owners described above. That way, Linux can e.g. allow me as a user to view my images, while preventing my colleague, who works on the same computer, to see them.

Every file and directory has the following three permissions defined:

1. *Read*: The read permission allows us to open and read a file, as well as to list the content of a directory. This permission is abbreviated with an `r`.

2. *Write*: The write permission allows us to modify the content of a file, as well as to add, remove or rename files in a directory. This permission is abbreviated with an `w`.

3. *Execute*: The execute permission allows us - big surprise - to execute a file. This permission is abbreviated with an `x`.

### Viewing permissions

With this knowledge in mind, let's now inspect the first column from the `ls -l` command above.

![](Figures/figure_permissions.png){width=30%}

This column encodes the permissions given to the user, group and others 

* The very first letter is the *file type*. The `-` implies that we have selected a file. A directory is encoded by a `d`.

* The next three letters are the permissions for the *user*. In our example, `rw-` means that the user can read and write, but not execute the file.

* The next three letters are the permissions for the *group*. As discussed above, Linux will by default add the user to a group with the same name as the user. Therefore, the group has by default also the same permissions as the user, in our example again `rw-`.

* The last three letters are the permissions for the *others*. In our example, `r--` means that the others can only read, but not write nor execute.

## Changing permissions

Permissions can be changed with the `chmod` command. 

To add or remove a permission, use `+` or `-`, followed by the permission we would like to change (`r`, `w` or `x`). 

For example, let's add the executing permission to all owners:

```{bash}
chmod +x test.sh
ls -l test.sh
```

... and remove it again:

```{bash}
chmod -x test.sh
ls -l test.sh
```

To change permissions of only one owner, add the owner in front (`u` for user, `g` for group, `o` for other).

For example, let's remove read permission from other:
```{bash}
chmod o-r test.sh
ls -l test.sh
```

and remove write permissions from group:

```{bash}
chmod g-w test.sh
ls -l test.sh
```

Back to our original problem: How can we run `test.sh`? To run a script, the user (me) needs to have executing rights. Currently, we do not have this, but we can easily add it with:

```{bash}
chmod u+x test.sh
ls -l test.sh
```

Now, let's run the script!

```{bash}
./test.sh
```

## Paths

You might have wondered why we write `./test.sh` instead of just `test.sh` for executing the script. The reason is simple: BASH wouldn't have found the file otherwise!

```{bash}
./test.sh
```

```{bash, error=TRUE}
test.sh
```

But why? 

When calling an executable such as test.sh, BASH looks for it in a list of directories that are stored in a variable called `$PATH`. This variable typically contains the following directories (separated by a colon `:`):

```{bash}
echo $PATH
```

BASH only looks for the executable in those directories and does not consider our current directory. Since `test.sh` is not located in one of these directories, BASH can not find it.  We need to tell BASH the unique location of the file inside the filesystem - the so-called path.  A path is how we refer to files and directories: it gives us the location of a file or a directory in the Linux directory system. 

As a user, we need to know the path if we want to access a certain file or directory. We can specify the path as absolute or relative:

1. __Absolute path__: The absolute path specifies the location of a file from the root directory. Everything on a Linux system is located under the root directory. The root directory is the top-most directory of all other directories, like the trunk of a tree where all branches originate from. It is represented with a `/`. 
  After the `/`, the absolute path lists all directories that need to be entered in order to get to the file, separated by `/`. We can get the absolute path with the command `pwd`:

  ```{bash}
  pwd
  ```

  We can run our script by pasting this path in front of its name:

  ```{bash, echo = FALSE}
  # Note: little hack such that it looks like we manually pasted pwd with test.sh. Didn't want to hardcode it, since other people might compile and path wouldn't match
  wd=$(pwd)
  fullPath=${wd}/test.sh 
  echo \$ $fullPath
  bash $fullPath
  ```
  
2. __Relative path__: This is the path relative to the current location. For relative paths, we often use the following three symbols:
  * `.` (a single dot): represents the directory we're in.
  * `..` (a double dot): represents the parent directory (i.e. one level above).
  * `~` (tilde): represents the home directory. This is the default directory that occurs after logging in. Most of our files will be located somewhere in here.
  
  Let's make this clear with an example. In our current directory, create two new directories.
  
  ```{bash}
  mkdir dir1 dir2
  ```
  We can change directories using `cd`:
  
  ```{bash}
  cd dir1
  touch file1.txt
  ls
  ```
  Let's now go to dir2 (via the parent directory), and copy file1 from dir1 here:
  
  ```{bash, echo = -1}
  cd dir1; # this is hidden in output
  cd ../dir2
  cp ../dir1/file1.txt .
  ls
  ```
  Finally, move file1 from dir1 to the parent directory:
  ```{bash, echo = -1}
  cd dir2; # this is hidden in output
  mv ../dir1/file1.txt ../movedFile.txt
  ls ../dir1
  ```
  
  ```{bash, echo = FALSE}
  # remove directories and files
  rm -r dir1 dir2
  ```
  
We have only used relative paths in this example. They are very handy, since they make code much more readable: suppose we had to replace each relative path above with its absolute path, it would have been a mess!
  
In summary: The absolute path always starts with the root `/` directory and is independent of our current location. The relative path depends on our current location.
  
## Passing arguments to a script

When running a script on the command line, we can pass arguments to it. Inside our script, we can use these arguments to e.g. write output to a specific directory or calculate something. There are pre-defined variables which we can use to access the command line arguments. `$0` refers to the first argument on the command line - which is simply the name of the BASH script we're executing. `$1` refers to the second argument, `$2` to the third, and so on.

For example, create a BASH script called "testArguments.sh" with the following content:

```{bash, prompt=FALSE, eval=FALSE}
#!/bin/bash
echo "Argument 0: $0"
echo "Argument 1: $1"
echo "Argument 2: $2"
echo "Argument 3: $3"
```

```{bash, echo = FALSE}
# actually write the script, hidden for students
echo "#!/bin/bash" > testArguments.sh
echo "echo \"Argument 0: \$0\"" >> testArguments.sh
echo "echo \"Argument 1: \$1\"" >> testArguments.sh
echo "echo \"Argument 2: \$2\"" >> testArguments.sh
echo "echo \"Argument 3: \$3\"" >> testArguments.sh
```

and execute it: 

```{bash}
chmod u+x testArguments.sh
./testArguments.sh is awesome
```


## Exercises

```{exercises}
Permission systems
```

Consider the three users chimp, gorilla and alien. For each user, there is a group with the same name, i.e. chimp belongs to group chimp; gorilla belongs to group gorilla, and alien belongs to group alien. In addition, chimp and gorilla both belong to group ape. 

![](Figures/figure_exercise_permissions.png){width=100%}

For the following examples, list all users that have 1) read permissions, 2) write permissions and 3) execute permissions.
Hint: the user and group permissions always supersede lower (i.e. other) permissions.


The first column represents permissions, the second column represents user, the third column represents group:

  `Permissions` `User` `Group`

1) `-r--r-----` `chimp` `chimp`

```{solutionBlock}
Read permissions: chimp
Write permissions: None
Execute permissions: None
```

2) `-rwxrw-r--` `chimp` `ape`

```{solutionBlock}
Read permissions: chimp, gorilla, alien
Write permissions: chimp, gorilla
Execute permissions: chimp
```

3) `-r-xr--rwx` `alien` `ape`

```{solutionBlock}
Read permissions: chimp, gorilla, alien
Write permissions: None
Execute permissions: alien
```

```{exercises}
Changing Permissions
```

Note: If you are on Windows and using the Ubuntu application, make sure to leave the directory that is under control of Windows, because Windows will not let you change permissions. Simply type `cd`, and it will jump to your Linux home directory. In there, you should be able to modify permissions.

1) Create a file "myPrecious.txt" and use `echo` to write the text "I and I alone!" into it. Make sure you are the only one allowed to read or write the file. Check the permissions using `ls`.

```{solutionBlock}
echo "I and I alone!" > myPrecious.txt
chmod -rw myPrecious.txt
chmod u+rw myPrecious.txt
ls -l
```

2) Change the permissions again to make sure that only the group permissions are set to read and write. Check that they are correct. Try to print the file content to the screen.

```{solutionBlock}
chmod -rw myPrecious.txt
chmod g+rw myPrecious.txt
ls -l
cat myPrecious.txt
# Note: you should get a "Permission denied" error, since you as a user do not have read permissions.
```

3) Remove the file myPrecious.txt

```{solutionBlock}
rm myPrecious.txt
# Since you do not have read and write rights but still are the owner of the file, you can delete it, but must confirm.
```

4) Create a directory "It’s all yours!". Create a file "yours.txt" inside that directory. Check it is there.

```{solutionBlock}
mkdir "It’s all yours!"
touch It\'s\ all\ yours\!/yours.txt
ls "It’s all yours!"
# Instead of using quotes, you can also escape the spaces and the exclamation mark. E.g.:
ls It’s\ all\ yours\!
```

5) Remove execution rights for yourself and try to enter the directory. Can you still see the files inside the
directory?

```{solutionBlock}
chmod u-x "It’s all yours!"
cd "It’s all yours!"
# Note: you should get a "Permission denied" error.
ls "It’s all yours!"
# Note: you should be able to see yours.txt, but also get an error that you can not access the directory itself.
```

6) Remove directory "It’s all yours!"

```{solutionBlock}
chmod u+x "It’s all yours!"
rm -r "It’s all yours!"
# Note: you need execution rights to recursively delete a directory! This is because you need to enter the directory first to delete its content.
```

7) Use `vim` to create a script "love.sh" that prints "I like BASH!" to STDOUT, and execute it 100 times.

```{solutionBlock}
vim love.sh
#!/bin/bash
echo "I like BASH!"
# Leave vim by pressing Esc, then :wq
chmod u+x love.sh
for i in `seq 1 100`; do ./love.sh; done
```

```{exercises}
Writing BASH Scripts
```

1) Write a script called "dog.sh" that takes three arguments: the name, the age and the color of a dog. In your script, print "Here comes ..., a ... year old dog of ... color.", where you replace ... with the arguments above. Call your script with two of your favourite dogs.

```{solutionBlock}
vim dog.sh
#!/bin/bash
echo "Here comes $1, a $2 years old dog of $3 color."
# Leave vim by pressing Esc, then :wq
chmod u+x dog.sh
./dog.sh "Max" "15" "brown"
./dog.sh "Selma" "5" "blond"
```

1) Use `vim` to create a script "like.sh" that takes an argument and prints "I like ...!" where ... is the argument passed. Use a for loop to write "I like biology!", "I like computer science!" and "I like bioinformatics!".

```{solutionBlock}
vim like.sh
#!/bin/bash
echo "I like $1!"
# Leave vim by pressing Esc, then :wq
chmod u+x like.sh
# you can call your script three times like this:
./like.sh "biology"
./like.sh "computer science"
./like.sh "bioinformatics"
# or alternatively, you write a for-loop:
for x in "biology" "computer science" "bioinformatics"; do ./like.sh "$x"; done
```

1) Write a script called "reasons.sh" that takes an argument and appends it to a file called "whyILikeBASH.txt". Call your script with three reasons why you like BASH (if you can't come up with a reason, you can also lists reasons why don't like it, and hopefully change your mind soon). Check the file.

```{solutionBlock}
vim reasons.sh
#!/bin/bash
echo $1 >> whyILikeBASH.txt
# Leave vim by pressing Esc, then :wq
chmod u+x reasons.sh
# you can call your script three times like this:
./reasons.sh "powerful"
./reasons.sh "flexible"
./reasons.sh "fast"
# or alternatively, you write a for-loop:
for x in "powerful" "flexible" "flexible"; do ./reasons.sh "$x"; done
cat whyILikeBASH.txt
```

2) Write a script called "append.sh" that takes two arguments: a string and a name of a file to which the string
is to be appended. Use it to write the words "Bern", "Fribourg", "Lausanne" to a file called "words.txt". Check
the file.

```{solutionBlock}
vim append.sh
#!/bin/bash
echo $1 >> $2
# Leave vim by pressing Esc, then :wq
chmod u+x append.sh
for n in Bern Fribourg Lausanne; do ./append.sh $n words.txt; done
cat words.txt
```

3) Write a script called "append2.sh" that takes two arguments: a string and a name of a file to which the string
is to be appended. Your script should first test if the file (given by the second argument) exists. If it does not, the script should create it and write "Created by append.sh" to it. Call your script to write three words of your choice to a file called "moreWords.txt". Check the file.

```{solutionBlock}
vim append2.sh
#!/bin/bash
if [ ! -e $2 ]
then echo "Created by append2.sh" > $2
fi
echo $1 >> $2
# Leave vim by pressing Esc, then :wq
chmod u+x append2.sh
for n in hungry thirsty sleepy; do ./append2.sh $n moreWords.txt
done
cat moreWords.txt
```

4) Create a script "positive.sh" that takes a number as an argument. If the number is larger than zero, it should write the number to a file. The name of that file should be yourNumber.txt (for example, if your number is 5, the file is called 5.txt). Call your script with the numbers -10, 0 and 10, and check if only a file called "10.txt" has been written.

```{solutionBlock}
vim positive.sh
#!/bin/bash
if [ $1 -gt 0 ]
then echo $1 > "${1}.txt"
fi
# Leave vim by pressing Esc, then :wq
chmod u+x positive.sh
for n in "-10" "0" "10"; do ./positive.sh $n
done
ls
```

4) Create a script "explain.sh" that goes through all files and directories in the current directory and prints the name followed by "is a directory" or "is a file". Create some extra directories and files before running it.

```{solutionBlock}
#!/bin/bash
for name in `ls`
do if [ -d $name ]
then echo "$name is a directory"
else echo "$name is a file"
fi
done
# Leave vim by pressing Esc, then :wq
chmod u+x explain.sh
mkdir testdir1 testdir2
touch testfile1 testfile2
./explain.sh
```

5) Create a script "helper.sh" that 1) writes a script "print.sh" and 2) executes it. The script print.sh shall then take two arguments: a string and a number, and it should print the string number times to STDOUT. Finally,
the script helper.sh should take the same arguments and forward them to print.sh when executing it. Run
it to print 10 times "Gotcha!".

```{solutionBlock}
vim helper.sh
#!/bin/bash
(
echo "#!/bin/bash"
echo "for i in \`seq 1 \$2\`"
echo "do echo \$1"
echo "done"
) > print.sh
chmod u+x print.sh
./print.sh $1 $2
# Leave vim by pressing Esc, then :wq

chmod u+x helper.sh
./helper.sh "Gotcha!" 10
```

