# Conditionals and Loops

![](Figures/daemonChapter5.png)

## Conditionals

__The__ if [..] .. then .. else .. fi __statement__  


This construction allows to execute commands conditional on the evaluation of other expressions. They are to be written on different lines or separated by semicolons (;).

Declaring variables from output
```{bash, prompt = F, eval = F}
if [ "foo" = "foo" ]; then echo "true!"; fi
#true!
if [ 1 > 20 ]; then echo "true!"; else echo "false!"; fi
#true!
if [ 1 -gt 20 ]; then echo "true!"; else echo "false!"; fi
#false!
```


By default, the expression is evaluating the arguments as strings. To force a numerical evaluation, __-lt__ (<), __-gt__ (>), __-le__ (<=), __-ge__ (>=), __-eq__ (==) or __-ne__ (!=).  



__File test operators__

_BASH_ offers built-in tests for the status of files. 

  * The most important are:

    - -e tests if a file / directory exists
    - -f tests if it is a regular file (no directory)
    - -d tests if it is a directory
    - -s tests if the size of a file is > zero

File test operators
```{bash, prompt = F, eval = F}
if [ -f out.txt ]; then echo "true"; else echo "false"; fi
#true
if [ -d out.txt ]; then echo "true!"; else echo "false"; fi
#false!
```

__Testing for not__  


To test for the opposite, simply put an exclamation mark ! before the operator.

File test operators
```{bash, prompt = F, eval = F}
if [ ! -f out.txt ]; then echo "true"; else echo "false"; fi
#false
if [ ! 10 -gt 100 ]; then echo "true!"; else echo "false"; fi
#true!
```


## For Loops

__The__ _for_ __loop__  

The _for_ loop let’s you iterate over a series of ’words’ within a string. The basic syntax consists the commands _for_, _do_ and _done_. 


for loops
```{bash, prompt = F, eval = F}
for i in hello world; do echo $i; done
#hello
#world
x="one two three"
for y in $x
do echo $y | sed ’s/t/T/’
done
#one
#Two
#Three
```


## While Loops

__The__ _while_ __loop__

_while_ executes code while the control expression is true and until it is false.  


The while loop
```{bash, prompt = F, eval = F}
COUNTER=0
while [ $COUNTER -lt 3 ]; do
echo "The counter is $COUNTER"
let COUNTER=COUNTER+1
done
#The counter is 0
#The counter is 1
#The counter is 2
```


We can also use a while loop to read over all lines of a file. For that we need to use "while read" follow by a variable name and use the operator "<" to redirect the _STDIN_ to a file. Keep in mind, in this case a counter is not needed since the looping while be over once we reach the end of the file.
```{bash, prompt = F, eval = F}
while read line; do
echo "Line: $line"
done < ls.txt
#Line: ls.all
#Line: ls.errl
#Line: ls.txt
#Line: out.txt
```
The variable line contains every time a new line from ls.txt (input is redirected with <)

## Evaluation of commands on the fly

__Capturing__ _STDOUT_ __as string__ 


_BASH_ offers an easy way to evaluate a specific command and then using the resulting _STDOUT_ for further evaluation: simply put backticks ` or $() around the command.  

Evaluate commands on the fly
```{bash, prompt = F, eval = F}
for i in seq 1 3; do echo $i; done
#seq
#1
#3
for i in `seq 1 3`; do echo $i; done
#1
#2
#3
for i in $(seq 1 3); do echo $i; done
#1
#2
#3
echo "I like `echo "2+5" | bc`!"
#I like 7!
if [ 1 -gt $(echo "3 - 4" | bc) ]; then echo "true!"; else echo "false!"; fi
#true!
```
If we don't provide the backstick or $(), the for loop will take the given command as a list of strings instead of evaluating it.

## Exercises

```{exercises}
Conditionals
```

1. Declare a variable `x` and set it to 5. Write an `if` statement that prints "indeed" if `x`is smaller than 10 and "nope" otherwise. Then set `x` to 20 and run your statement again.
```{solutionBlock}
x=5
if [ $x -lt 10 ]; then echo "indeed"; else echo "nope"; fi
x=10
#use arrow up to run the if statement again
```

1. Write an `if` statement that prints "is a file" if a variable `that` is indeed a file, "is a directory" if it is a directory and nothing if it is neither (i.e. it does not exists).  Create a file FileA.txt and a directory "myDir". Declare the variable `that` and set it to "FileA.txt", "myDir" and "FileB.txt" to test if your `if` statement does the right thing. Hint: you need a nested if statement.
```{solutionBlock}
touch FileA.txt
mkdir myDir
that=FileA.txt
if [ -f $that ]; then echo "is a file"; else if [ -d $that ]; then echo "is a directory"; fi; fi
that=myDir
#use arrow up to run the if statement again
that=FileB.txt
#use arrow up to run the if statement again
```

```{exercises}
Loops
```

1. Go to directory chimp (previously created in Exercises Chapter 2), write a loop to add the words ”bananas”, ”mangos” and ”ants” to the file food.txt
```{solutionBlock}
for i in bananas mangos ants; do echo $i; done >> food.txt
```

2. Write a loop to add the word ”banana” 100 times to food.txt
```{solutionBlock}
for i in `seq 1 100`; do echo "banana"; done >> food.txt

Can also be done using while:
counter=0; while [ 100 -gt $counter ]; do echo "banana" >> food.txt; let counter=counter+1; done
```

3. Declare a variable `n` and set it to "1 2 3". Then write a `for` loop that loops over `n` and prints the first i lines of food.txt for each entry i in `n` (i.e. for i equal to 1 2 and 3). Now set `n` to "10 20 30" and rerun your loop.
```{solutionBlock}
n="1 2 3"
for i in $n; do head -n $i food.txt; done
n="10 20 30"
# use arrow up to rerun the same for loop again
```

3. Write a loop that writes 100 times ”ACGT” to DNA.txt, but all 100 times on the same line. Hint: check the man pages for echo to find a way!
```{solutionBlock}
for i in ‘seq 1 100‘; do echo -n "ACGT"; done >> DNA.txt

Can also be done using while:
counter=0; while [ 100 -gt $counter ]; do echo "ACGT"; let counter=counter+1; done >> DNA.txt
```


4. Write a loop that reads each line of food.txt and prints it, starting with ”Line n = ” followed by the actual line. Here, n is the number of the line (Line 1 = , Line 2 = , ...)
```{solutionBlock}
num=1; while read line; do echo "Line $num = $line"; let num=num+1; done < food.txt
```

5. Write a `for` loop that creates directories "myDir_1", "myDir_2", ..., "myDir_10". 
```{solutionBlock}
for i in `seq 1 10`; do mkdir myDir_$i; done
```

6. Write a `for` loop that writes "I'm in directory DIR" to a file "DIR.txt" into each directory starting with "myDir_", where DIR corresponds to the name of the directory.
```{solutionBlock}
for d in myDir_*; do echo "I'm in directory $d" > $d/$d.txt; done
```

7. Create a directory "all". Then write a `for` loop that loops over all directories starting with "myDir_", enters that directory, moves all files in there to the directory all and exists the directory again. Can you also come up with a solution without using a loop?
```{solutionBlock}
mkdir all
for d in myDir_*; do cd $d; mv * ../all; cd ..; done
#solution without loop: mv myDir_*/* all
```

8. Write a `for` loop that creates 100 files names "File_1.txt", "File_2.txt" and so forth. Each file "File_x.txt" should contain exactely x lines reading "Line 1", "Line 2", ..., "Line x". For instance, file "File_4.txt" should have the four lines "Line 1", "Line ", "Line 3* and "Line 4". Hint: use nested loops.
```{solutionBlock}
for i in `seq 1 100`; do for j in `seq 1 $i`; do echo "Line $j"; done > File_$i.txt; done
```

9. Write a `for` loop that loops over all files "File_1.txt", "File_2.txt", ... and appends the line "This file has x lines" to them, where x should match the actual number of lines. Hint: use `wc -l FILE | awk '{print $1}'` to get the number of lines of a file FILE. We will learn these commands later.
```{solutionBlock}
for file in File_*.txt; do n=$(wc -l $file | awk '{print $1}'); echo "This file has $n lines" >> $file; done
```


9. Use a `while` loop to read each line of file "File_5.txt" and print "File_5.txt, line x: LINE" to screen, where x is the line number and LINE is the actual line of that file.
```{solutionBlock}
i=1; while read line; do echo "File_5.txt, line $i: $line"; let i=i+1; done < File_5.txt
```

10. Use a `for` loop to run the above solution (printing the file name and line number in front of the line) for all files matching "File_*.txt". Write the output for any file "File_x.txt" to "File_x.txt.annotated".
```{solutionBlock}
for file in File_*.txt; do
i=1;
while read line; do
echo "$file, line $i: $line";
let i=i+1;
done < $file > $file.annotated
done
```

10. Use `ls` to create a file allFiles.txt that contains the names of all files "File_2.txt", "File_3.txt", File_12.txt", File_13.txt", "File_22.txt", "File_23.txt", "File_32.txt",..., "File_93.txt". Then use a `while` loop to create a file someLines.txt that contains the first 7 lines of all files listed in allFiles.txt.
```{solutionBlock}
ls File_*[2,3].txt > allFiles.txt
while read line; do head -n7 $line; done < allFiles.txt > someLines.txt
```

5. [Challenge] Use a `for` loop to print out the first 50 values of the Fibonacci series 1, 1, 2, 3, 5, 8, 13, .... Hint: use a variable to store the previous two values.
```{solutionBlock}
i=0; j=1; 
for k in `seq 1 50`; do echo "$j"; z=$(echo "$i+$j" | bc); i=$j; j=$z; done
```



![;)](Figures/bashTerminator.jpg)
